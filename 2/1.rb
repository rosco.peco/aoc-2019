if ARGV.length != 1
  puts 'Usage: 1.rb <filename.int>'
else
  code = File.read(ARGV.first).split(',').map(&:to_i)
  ip = 0

  while true
    puts "#{ip} => #{code.inspect}"
    
    a = code[code[ip+1]]
    b = code[code[ip+2]]
    
    case code[ip]
    when 1
      # OPADD
      puts "#{a} + #{b}"
      code[code[ip+3]] = a + b
    when 2
      # OPMUL
      puts "#{a} * #{b}"
      code[code[ip+3]] = a * b
    when 99
      break
    end

    ip += 4
  end

  puts "Result: \e[1;37m#{code[0]}\e[0m"
end

