# Just naive brute force, fine for 9801 combinations...
if ARGV.length != 2
  puts 'Usage: 1.rb <filename.int> <target>'
else
  filename, target = ARGV.first, ARGV.last.to_i

  99.times do |a|
    99.times do |b|
      code = File.read(filename).split(',').map(&:to_i)
      ip = 0
      code[1], code[2] = a, b

      while true
        case code[ip]
        when 1
          # OPADD
          code[code[ip+3]] = code[code[ip+1]] + code[code[ip+2]]
          ip += 4
        when 2
          # OPMUL
          code[code[ip+3]] = code[code[ip+1]] * code[code[ip+2]]
          ip += 4
        when 99
          ip += 1
          break
        end
      end
      
      puts "#{a}, #{b} => #{code[0]}"

      if code[0].eql?(target)
        puts "Fin"
        exit
      end
    end
  end
end



  
  

