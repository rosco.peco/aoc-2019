def calc_fuel(input)
  cost = (input / 3 - 2).to_i

  p cost

  if (cost < 0)
    0
  else
    cost + calc_fuel(cost)
  end
end



puts File.read('input.txt').each_line.map { |l| l.to_i }.map { |n| calc_fuel(n) }.sum

