if ARGV.length != 1
  puts "Usage: 1.rb <input.txt>"
else
  CENTRALX = 10000
  CENTRALY = 10000
  
  def plot_wire(wire)
    path = [[CENTRALX, CENTRALY]]

    wire.each do |cmd|
      dir = cmd[0]
      len = cmd[1..-1].to_i

      newx = path.last[0]
      newy = path.last[1]

      len.times do
        case dir
        when 'L'
          newx = newx - 1
        when 'R'
          newx = newx + 1
        when 'U'
          newy = newy - 1
        when 'D'
          newy = newy + 1
        else 
          raise ArgumentError, "Invalid direction: #{dir}"
        end
        
        path << [newx, newy]
      end
    end

    path[1..-1] # exclude first element (central point)
  end

  def manhattan_distance(x1, y1, x2, y2)
    (x2 - x1).abs + (y2 - y1).abs
  end

  paths = File.read(ARGV.first).each_line
    .map { |l| l.chomp.split(',') }
    .reject(&:empty?)
    .map { |i| plot_wire(i) }

  puts "Have \e[1;33m#{paths.length}\e[0m paths..."

  if paths.length != 2
    puts "\e[1;31mMalformed input; Need two paths\e[0m"
    exit
  end

  # intersection works for two wires...
  intersections = paths.first & paths.last

  puts "Have \e[1;33m#{intersections.length}\e[0m intersections..."

  distances = intersections.map { |(x,y)| manhattan_distance(CENTRALX, CENTRALY, x, y) }

  puts "Distances are \e[1;34m#{distances.inspect}\e[0m" if distances.length < 10
  puts "Answer: \e[1;32m#{distances.min}\e[0m"
end

